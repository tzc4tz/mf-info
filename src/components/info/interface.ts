export interface UserInfoProps {
  username: string;
  address: {
    street: string;
    city: string;
    zipcode: string;
    geo: { lat: string; lng: string };
    suite: string;
  };
  company: { name: string; catchPhrase: string; bs: string };
  email: string;
  id: number;
  name: string;
  phone: string;
}
