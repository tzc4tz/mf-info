import React, { useEffect, useState } from "react";
import styles from "./info.module.scss";
import { UserInfoProps } from "./interface";

interface Props {
  onLogOut?: () => void;
}

const Info = ({ onLogOut }: Props) => {
  const [userInfo, setUserInfo] = useState<UserInfoProps | null>();

  const fetchUserInfo = (username: UserInfoProps["username"]) => {
    fetch("https://jsonplaceholder.typicode.com/users/1")
      .then((res) => res.json())
      .then((data) => {
        const newData = { ...data, username };
        sessionStorage.setItem("data", JSON.stringify(newData));
        setUserInfo(newData);
      });
  };

  const logOut = () => {
    sessionStorage.removeItem("data");
    onLogOut?.();
  };

  useEffect(() => {
    const data = JSON.parse?.(sessionStorage.getItem("data") as string);
    if (data?.id) {
      setUserInfo(data);
      return;
    }
    fetchUserInfo(data?.username);
  }, []);

  return (
    <div className={styles["info__wrapper"]}>
      {userInfo && (
        <div className={styles["info"]}>
          <div className={styles["info--name"]}>{userInfo.name}</div>
          <div className={styles["info__rows"]}>
            <span>e-mail</span>
            <span>{userInfo.email}</span>
          </div>
          <div className={styles["info__rows"]}>
            <span>phone</span>
            <span>{userInfo.phone}</span>
          </div>
          <div className={styles["info__rows"]}>
            <span>street</span>
            <span>{userInfo.address.street}</span>
          </div>
          <div className={styles["info__rows"]}>
            <span>city</span>
            <span>{userInfo.address.city}</span>
          </div>
          <div className={styles["info__rows"]}>
            <span>state</span>
            <span>{userInfo.address.city}</span>
          </div>
          <div className={styles["info__rows"]}>
            <span>postcode</span>
            <span>{userInfo.address.zipcode}</span>
          </div>
          <div className={styles["info--username"]}>
            USERNAME {userInfo.username}
          </div>
          <div className={styles["info__logout"]}>
            <button onClick={logOut}>Log out</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Info;
