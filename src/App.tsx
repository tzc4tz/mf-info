import React from "react";
import ReactDOM from "react-dom";
import Information from "./components/info";

import "./index.scss";

const App = () => <Information />;

ReactDOM.render(<App />, document.getElementById("infoRoot"));
